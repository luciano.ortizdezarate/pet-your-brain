import ClockView from "../components/clock.component"
import MusicDashboard from "../components/music.component"
import Pet from "../components/pet.component"
import ToDoList from "../components/todolist.component"
function MainView(){
    return(
        <div className="container">
            <div className="row">
                <div className="col">
                    <Pet/>
                </div>
                <div className="col">
                    <ClockView/>
                </div> 
                <div className="col" >
                    <ToDoList/>
                </div>     
            </div>
            <MusicDashboard/> 
        </div>
    )
}
export default MainView