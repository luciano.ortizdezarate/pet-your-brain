import MainView from './views/mainView.view';
import './App.css';
import './stylesSheet/background.style.css'
import './components/firebase.component'

function App() {

  return (
    <div className="App" style={{marginTop:50}}>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <div className="firefly"></div>
      <MainView/>
    </div>
  );
}

export default App;
