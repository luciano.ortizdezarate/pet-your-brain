import {Button, Form, Modal, Nav, Navbar} from 'react-bootstrap'
import { useState } from 'react';
import '../stylesSheet/imagesLog.style.css'
import  {registro}   from './signUp.component';


export default function Header() {
    //Hooks para Modal Registro
    const [showR, setShowR] = useState(false);
    const handleCloseR = () => setShowR(false);
    const handleShowR = () => setShowR(true);
    //Hooks para Modal Acceso
    const [showA, setShowA] = useState(false);
    const handleCloseA = () => setShowA(false);
    const handleShowA = () => setShowA(true);
    //Hooks para Acceso por Usuario
    const [showU, setShowU] = useState(false);
    const handleCloseU = () => setShowU(false);
    const handleShowU = () => setShowU(true);
    //Hooks para firebase
    const [registro_user, setRegistro_user] = useState('');
    const [registro_pass, setRegistro_pass] = useState('');


    return (
    //Componente de header 
    <Navbar bg="dark" variant="dark">
        <Navbar.Brand href="/">
            <h1 style={{textAlign:"center"}}>
                <img src='brain.png' alt="" style={{width:'10%', height:'10%'}}/>
                PET YOUR BRAIN
            </h1>
        </Navbar.Brand>
            
        <Navbar.Collapse id="navbarSupportedContent"/*>
            <ul </div>className="navbar-nav ml-auto" */>
            <Nav.Item /*li className="nav-item loggedOut" style = {{display: "none"}}*/>
                <Nav.Link id="navlinks" /*href="#"*/ data-toggle="modal" data-target="#registroModal" 
                    onClick={() =>{
                        handleShowR()}}>
                        Registrarse
                </Nav.Link>
            </Nav.Item>
            <Nav.Item /* li className="nav-item loggedOut" style = {{display: "none"}}*/>
                <Nav.Link id="navlinks" /*href="#"*/ data-toggle="modal" data-target="#accesoModal"
                    onClick={() =>{
                        handleShowA()}}>
                        Acceder
                </Nav.Link>
            </Nav.Item>
            <Nav.Item /* li className="nav-item loggedIn" style = {{display: "none"}}*/>
                <Nav.Link id="navlinks" href="#" /*id ="logout"*/>Cerrar Sesión</Nav.Link>
            </Nav.Item >
            
        </Navbar.Collapse>
    
    <Modal show={showR} onHide={handleCloseR} animation={false}/*className="modal fade" id="registroModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"*/>
        <Modal.Header closeButton>
                <Modal.Title>Registrate!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
                    <Form id ="registro-form">
                        <Form.Group>
                            <Form.Control type = "email" id = "signup-email" placeholder="example@domain.com" onChange={(element)=>{setRegistro_user(element.target.value)}} required /> 
                        </Form.Group>
                        <Form.Group>
                            <Form.Control type = "password" id = "signup-password" placeholder="**********" onChange={(element)=>{setRegistro_pass(element.target.value)}} required />
                        </Form.Group>
                    </Form>
                    <Modal.Footer>
                        <Button type="button" variant="primary" onClick={()=> registro(registro_user,registro_pass)}>Registrarse</Button>
                    </Modal.Footer>
        </Modal.Body>
    </Modal>

    <Modal show={showA} onHide={handleCloseA} animation={false} /*className="modal fade" id="accesoModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"*/>
        <Modal.Header closeButton>
            <Modal.Title>Entra a tu cuenta</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <ul><Button type="Button" variant="light" className="btn btn btn-lg " id ="google-login">Acceder con <img id="logo_google" alt="logo_google" src='gmail_logo.png'></img></Button></ul>
            <ul><Button type="Button" variant="light" className="btn btn btn-lg " id ="fb-login">Acceder con <img id="logo_fb" src='facebook_logo-2.png' alt="fb_logo"></img></Button></ul>
            <ul><Button onClick={() =>{
                handleShowU()}}
                type="Button" 
                variant="light" 
                className="btn btn btn-lg " 
                id ="user-login" 
                data-toggle="modal" 
                data-target="#modal_user">
                    <a href="/#">Acceder con Usuario <img id="logo_user" alt="logo_user" src='logo_user.png' ></img> </a>
                </Button>
            </ul>
        </Modal.Body>
    </Modal>

    <Modal show={showU} onHide={handleCloseU} animation={false}/*className="modal fade" id="modal_user" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"*/>
        <Modal.Header closeButton>
            <Modal.Title>Accede con tu Usuario</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form id ="login-form">
                <Form.Group>
                    <Form.Label>Usuario</Form.Label>
                    <Form.Control type = "email" id = "login-email" placeholder="example@domain.com" required></Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Contraseña</Form.Label>
                    <Form.Control type = "password" id = "login-password" placeholder="**********" required></Form.Control>
                </Form.Group>
            </Form>
            <div>¿Olvidaste tu contraseña?</div>
            <Modal.Footer>
                <ul ><Button type="Button" variant="dark" className="btn btn-lg btn-block" data-dismiss="modal" aria-label="Close" >Volver</Button></ul>
                <ul><Button type="submit" variant="primary" className="btn btn-lg btn-block">Acceder</Button></ul>
            </Modal.Footer>
        </Modal.Body>     
        </Modal>
    </Navbar>
);
}