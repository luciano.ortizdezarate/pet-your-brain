import firebase from "firebase/app";
import 'firebase/firestore'
import 'firebase/auth'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyChbTeFi0wweGKnu6-n6_L5jVDQT7R1BKc",
    authDomain: "pet-your-brain.firebaseapp.com",
    projectId: "pet-your-brain",
    storageBucket: "pet-your-brain.appspot.com",
    messagingSenderId: "6543060345",
    appId: "1:6543060345:web:bf314bd997b91b5f3313a8",
    measurementId: "G-KTJXGLFPQW"
};

// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig);
export const auth = fb.auth();
export const fstore = firebase.firestore();
// fb.analytics();
